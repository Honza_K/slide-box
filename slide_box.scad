/*
 * All dimensions are in mm.
 */
use <scad-modules/rounded_cube.scad>


// thickness of sides
floor_thickness=1.25;
ceil_thickness=1.25;
side_thickness=2.6;
front_thickness=2;
back_thickness=2.6;

inner_length=100.5;             /* inner length */
inner_width=50.39;              /* inner width */
inner_height=30.3;              /* inner height */
lid_height=10;                  /* total height of the lid (outer) */

outer_length=inner_length+front_thickness+back_thickness;
outer_width=inner_width+2*side_thickness;
outer_height=inner_height+floor_thickness+ceil_thickness;

/*
 * Text to put on back.
 */
text_label="0.9.4";

/*
 * Whether to put the text on lid and base
 */
text_on_lid=true;
text_on_base=true;

/*
 * How deep should the text label be inset
 */
text_inset=0.3;

/*
 * Set to true to create serrated area on lid for better grip when opening.
 */
lid_serrated=true;

/*
 * How much should the corners be rounded. Default value is 1/2 of the thinnest wall. Set to 0 to disable rounding.
 */
min_radius=min(floor_thickness, side_thickness, front_thickness, back_thickness,ceil_thickness)/2;

/*
 * How much to shrink base to allow lid to fit. Will be applied on planes in contact with lid.
 * Increase, if the lid cannot fit on base, decrease, if it is too loose.
 */
base_shrinkage=0.075;

/*******************************************************************************
 * End of variables to customize. You should not need to modify the code below.
 ******************************************************************************/
lid_overlap=side_thickness/2;
groove_thickness=lid_overlap/2;
groove_offset=2;

lock_offset=4;

epsilon=0.1;                    /* how much overlap objects to prevent non-orientable results */

module box_label(size, depth, txt)
{
     linear_extrude(height=depth) text(text=txt,size=size, $fn=20);
};


/*
 * @param count how many ridges in serrated area
 * @param ridge dimensions for single ridge in form [x, y, z], where x=length of single ridge (total length of serrated area will be count*ridge.x), y=height of serrated area, z=depth of a ridge.
 */
module serration(count, ridge)
{
     for (i=[0:count-1]) {
          linear_extrude(height=ridge.y)
               translate([ridge.x*i,-epsilon,0])
               polygon([[0,0],[0,ridge.z+epsilon],[ridge.x+epsilon,0]]);
     }

}

module lid()
{
     groove_z_offset=lid_height-groove_offset;
     difference() {
          union() {
               difference () {
                    // basic shape - full, solid
                    rounded_cube([outer_length, inner_width+2*side_thickness, inner_height], radius=min_radius);
                    // cut to proper height
                    translate([-epsilon,-epsilon, lid_height])
                         cube([outer_length+2*epsilon, outer_width+2*epsilon, outer_height]);
                    // make the lid hollow
                    translate([-back_thickness,side_thickness-lid_overlap,ceil_thickness])
                         cube([outer_length, inner_width+2*lid_overlap, inner_height]);
                    // cut front to match base overlap
                    translate([-front_thickness,-side_thickness,ceil_thickness])
                         cube([2*front_thickness, inner_width+4*side_thickness, inner_height+ceil_thickness]);
                    // cut back to match base overlap
                    translate([outer_length-back_thickness-epsilon,side_thickness-lid_overlap,lid_height/2])
                         cube([back_thickness/2+epsilon, inner_width+2*lid_overlap, lid_height]);

                    // engrave label
                    if (text_on_lid) {
                         translate([outer_length-text_inset,outer_width-side_thickness,lid_height-ceil_thickness])
                              rotate([90,180,90])
                              box_label(size=5, depth=text_inset+epsilon, txt=text_label);
                    }

                    // add serrated area to improve grip
                    if (lid_serrated) {
                         serration_depth=(side_thickness-lid_overlap)/2;
                         sr=[2, lid_height-3*min_radius, serration_depth];
                         translate([outer_length-front_thickness,0,2*min_radius]) mirror([1,0,0]) serration(20, sr);
                         translate([outer_length-front_thickness,inner_width+2*side_thickness,2*min_radius]) mirror([0,1,0]) mirror([1,0,0]) serration(20, sr);
                    }
               };
               // tongues for the grooves in base
               {
                    difference() {
                         translate([front_thickness,inner_width+side_thickness+lid_overlap,lid_height-groove_offset])
                              rotate([0,90,0])
                              cylinder(h=outer_length-front_thickness-epsilon, r=groove_thickness, $fn=4);
                         // gap in the tongues to create lock
                         translate([front_thickness+lock_offset,outer_width-side_thickness, lid_height-groove_offset])
                              sphere(lid_overlap, $fn=30);
                    }

                    difference() {
                         translate([front_thickness,side_thickness-lid_overlap,lid_height-groove_offset])
                              rotate([0,90,0])
                              cylinder(h=outer_length-front_thickness-epsilon, r=groove_thickness, $fn=4);
                         // gap in the tongues to create lock
                         translate([front_thickness+lock_offset,side_thickness, lid_height-groove_offset])
                              sphere(lid_overlap, $fn=30);
                    }
               }
          }
          translate([0,0,ceil_thickness])
               linear_extrude(height=lid_height)
               polygon([[0,0],
                        [front_thickness+lock_offset-lid_overlap,lid_overlap+groove_thickness],
                        [front_thickness+lock_offset-lid_overlap,outer_width-lid_overlap-groove_thickness],
                        [0,outer_width]]);
     }
}

module base()
{
     groove_z_offset=outer_height-lid_height+groove_offset;
     difference()
     {
          union() {
               difference() {
                    // basic shape
                    rounded_cube([outer_length, outer_width, outer_height], radius=min_radius);

                    // grooves
                    translate([front_thickness-epsilon,side_thickness-lid_overlap+base_shrinkage,groove_z_offset])
                         rotate([0,90,0])
                         cylinder(h=outer_length, r=groove_thickness, $fn=4);
                    translate([front_thickness-epsilon,outer_width-side_thickness+lid_overlap-base_shrinkage,groove_z_offset])
                         rotate([0,90,0])
                         cylinder(h=outer_length, r=groove_thickness, $fn=4);

                    // shrink sides
                    translate([front_thickness-base_shrinkage,-lid_overlap+base_shrinkage,outer_height-lid_height-base_shrinkage])
                         cube([outer_length, side_thickness,lid_height+base_shrinkage+epsilon]);
                    translate([front_thickness-base_shrinkage,outer_width-lid_overlap-base_shrinkage,outer_height-lid_height-base_shrinkage])
                         cube([outer_length, side_thickness,lid_height+base_shrinkage+epsilon]);

                    // shrink back
                    translate([outer_length-back_thickness/2-2*base_shrinkage,0,outer_height-lid_height-base_shrinkage])
                         cube([back_thickness+epsilon, outer_width,lid_height+base_shrinkage+epsilon]);

                    // shrink top
                    translate([-epsilon,-epsilon,outer_height-ceil_thickness-base_shrinkage])
                         cube([outer_length, outer_width+2*epsilon, ceil_thickness+base_shrinkage+epsilon]);
               }
               // locks in grooves
               translate([front_thickness+lock_offset,side_thickness, groove_z_offset])
                    sphere(lid_overlap, $fn=30);

               translate([front_thickness+lock_offset,outer_width-side_thickness, groove_z_offset])
                    sphere(lid_overlap, $fn=30);
          }
          // cut out inside
          translate([front_thickness, side_thickness, floor_thickness])
               cube([inner_length, inner_width, outer_height]);

          // cut out back
          translate([outer_length-back_thickness-base_shrinkage, 0, outer_height-lid_height/2-base_shrinkage])
               cube([2*back_thickness, outer_width, lid_height]);

          if (text_on_base)
          {
               // engrave label
               translate([outer_length-text_inset,ceil_thickness,side_thickness])
                    rotate([90,0,90])
                    box_label(size=5, depth=text_inset+epsilon, txt=text_label);
          }
     }
}
