# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- Rounded cube is now taken from submodule.
- Added lock to groove&tongue to fix the lid in closed position

## 0.9.3 - 2019-01-24
- Made the default lid fit tighter.
- Text on base and lid can be separately enabled/disabled.
- Fixed design error, when lid could not be slid fully on the base.

## 0.9.2 - 2019-01-23
- Changed ridge to work better and don't weaken the sides too much.
- Improved matching of base and lid.
- Back wall thickness is defined in separate variable.

## 0.9.1 - 2019-01-08
- Improved rendering time by removing minkowski().
- Added "grips" on lid.
- Rounded corners.

## 0.9.0
- Initial version.
